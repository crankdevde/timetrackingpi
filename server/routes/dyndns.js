var express 		= require('express'),
	SerialPort		= require('serialport'),
	Datastore		= require('nedb'),
	request 		= require('request'),
	bodyParser		= require('body-parser'),
	cookieParser	= require('cookie-parser'),
	expressJwt		= require('express-jwt'),
	jwt 			= require('jsonwebtoken'),
	session 		= require('express-session'),
	router  		= express.Router();

router 	.use(function(req,res,next){
			next();
		})

	 	.route('/')
	 	.get(function(req, res){
	 		res.json({status:200,message:'running'});

	 	});
module.exports.router = router;