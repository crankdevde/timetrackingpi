module.exports = function($routeProvider, $httpProvider,authInterceptor) {
	$routeProvider
		.when('/',{templateUrl: './views/dashboard.html', controller: 'DashboardController', requireAuth:true})
		.when('/timeline',{templateUrl: './views/timeline/list.html', controller: 'TimeLineController', requireAuth:true})
		.when('/timeline/:keyId',{templateUrl: './views/timeline/detail.html', controller: 'TimeLineController', requireAuth:true})
		.when('/timeline/:keyId/:year/:month',{templateUrl: './views/timeline/detail.html', controller: 'TimeLineController', requireAuth:true})
		.when('/login',{templateUrl: './views/login.html', controller: 'UserController', requireAuth:false})
		.when('/employee',{templateUrl: './views/employee/list.html', controller: 'EmployeeController', requireAuth:true})
		.when('/employee/:keyId',{templateUrl: './views/employee/detail.html', controller: 'EmployeeController', requireAuth:true})
		.when('/employee/:keyId/delete',{templateUrl: './views/employee/delete.html', controller: 'EmployeeController', requireAuth:true})
		.when('/employee/:keyId/edit',{templateUrl: './views/employee/detail.html', controller: 'EmployeeController', requireAuth:true})
		.when('/keys',{templateUrl: './views/keys/list.html', controller: 'KeyController', requireAuth:true})
		.when('/keys/:keyId',{templateUrl: './views/keys/detail.html', controller: 'KeyController', requireAuth:true})
		.when('/keys/:keyId/delete',{templateUrl: './views/keys/delete.html', controller: 'KeyController', requireAuth:true})
		.when('/keys/add',{templateUrl: './views/keys/edit.html', controller: 'KeyController', requireAuth:true})
		.otherwise({redirectTo: '/' });
	$httpProvider.interceptors.push('authInterceptor');
};