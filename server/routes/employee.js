var express 		= require('express'),
	SerialPort		= require('serialport'),
	Datastore		= require('nedb'),
	request 		= require('request'),
	bodyParser		= require('body-parser'),
	cookieParser	= require('cookie-parser'),
	expressJwt		= require('express-jwt'),
	jwt 			= require('jsonwebtoken'),
	session 		= require('express-session'),
	router  		= express.Router();

dbEmployee			= new Datastore({filename: 'employee.db', autoload: true});

router 	
	.use(function(req,res,next){
		next();
	});

router  
	.route('/employee')
 	.get(function(req, res){
 		dbEmployee.find({},function(err,docs) {
			if (err) console.log(err);
			res.json(docs);
		});
 	})
 	.post(function(req, res) {
		var doc = {};
		doc.firstname = req.body.firstname;
		doc.lastname = req.body.lastname;
		doc.sn = req.body.sn;
		doc.gender = req.body.gender;
		dbEmployee.insert(doc,function(err,newDoc){
			if( err ) console.log(err);
			res.json(newDoc);
		});
	});

router 	
	.route('/employee/:keyId')
 	.post(function(req, res) {
		var doc = {};
		doc.firstname = req.body.firstname;
		doc.lastname = req.body.lastname;
		doc.sn = req.body.sn;
		doc.gender = req.body.gender;
		dbEmployee.remove({sn:req.params.keyId},{ multi: true },function(err,resDoc){
			if( err ) console.log(err);
			dbEmployee.insert(doc,function(err,newDoc){
				if( err ) console.log(err);
				dbEmployee.find({},function(err,docs) {
					if (err) console.log(err);
					res.json(docs);
				});
			});
		});
		
	})
	.delete(function(req,res){
		dbEmployee.remove({sn:req.params.keyId},{ multi: true },function(err,doc){
			dbTime.remove({sn:req.params.keyId},{ multi: true },function(err,doc){
				res.json(doc);
			});
		});
	})
	;

module.exports.router = router;