module.exports = function($scope, $http, $window,$location,$routeParams) {

	$scope.aEmployee = {};
	$scope.listUser = [];
	$scope.employee = {firstname:'',lastname:'',sn:''};
	$scope.keyId = $routeParams.keyId;

	$scope.getList = function() {
		$scope.listUser = [];
		$http.get("/api/timeline/user").then(function (response) {
	        $scope.listUser = response.data;
		});
	};
	$scope.getList();

	$scope.delete = function(keyId) {
		$http.delete("/api/employee/"+keyId).then(function (response) {
	        $location.path('/employee');
		});
	};

	$scope.getEmployees = function() {
		console.log('getEmployees');
		$scope.aEmployee = {};
		$scope.employee = {firstname:'',lastname:'',sn:''};
		$http.get("/api/employee").then(function (response) {
			for(var emp in response.data) {
				$scope.aEmployee[response.data[emp].sn] = response.data[emp];
			}
			if ( $scope.keyId ) {
				$scope.employee = $scope.aEmployee[$scope.keyId];
			}
		});
	};
	$scope.getEmployees();

	$scope.update = function(employee) {
		$scope.master = angular.copy(employee);
		$http.post("/api/employee/"+$scope.keyId,employee).then(function (response) {
		    $scope.employee = response.data;
			$scope.viewForm = false;
			$scope.viewTimes = true;
			$window.location.reload();
		});
	};

	console.log($scope.aEmployee);

	$scope.go = function ( path ) {
	  $location.path( path );
	};
};