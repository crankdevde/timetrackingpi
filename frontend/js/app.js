require('angular');
jQuery = require('jquery');
var app = angular.module('TimeTrackingPi',[require('angular-route')]);
app.constant()
.config( ['$routeProvider','$httpProvider',require('./components/routes/config.js')])
.factory('authInterceptor', ['$q','$window',require('./components/user/Service')])
.controller('DashboardController',['$scope', '$http', '$window','$location','$routeParams',require('./components/dashboard/Controller')])
.controller('EmployeeController',['$scope', '$http', '$window','$location','$routeParams',require('./components/employee/Controller')])
.controller('RfidController',['$scope', '$http', '$window','$location','$routeParams',require('./components/rfidreader/Controller')])
.controller('TimeLineController',['$scope', '$http', '$window','$location','$routeParams',require('./components/timeline/Controller')])
.controller('UserController',['$scope', '$http', '$window','$location','$routeParams','authInterceptor',require('./components/user/Controller')])
.controller('KeyController',['$scope', '$http', '$window','$location','$routeParams','authInterceptor',require('./components/keys/Controller')])
.run(['$rootScope', '$location', 'authInterceptor', function ($rootScope, $location, authInterceptor) {                
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if (next.$$route.requireAuth) {
            if (!authInterceptor.getAuth()) {
                $location.path("/login");
            }                       
        }

    });
}])
;