module.exports = function($scope, $http, $window,$location,$routeParams) {
	$scope.paramKeyId = $routeParams.keyId;

	$scope.timelineEntry = {sn:'',time:''};
	$scope.go = function ( path ) {
	  $location.path( path );
	};

	$scope.delete = function(keyId) {
		$http.delete("/api/keys/"+keyId).then(function (response) {
	        $location.path('/');
		});
	};

	$scope.addEntry = function(){
		$scope.timelineEntry.time = new Date($scope.timelineEntry.time);
		$http.post("/api/keys",$scope.timelineEntry).then(function (response) {
	        $location.path('/timeline');
		});
	};
};