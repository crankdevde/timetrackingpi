module.exports = function($q,$window) {
	var isAuth = false;
	var user = {name:'',password:''};

	return {
		request: function(config) {
			config.headers = config.headers || {};
			if ( $window.sessionStorage.token) {
				isAuth = true;
				config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;		
				// disable IE ajax request caching
			    config.headers['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
			    // extra
			    config.headers['Cache-Control'] = 'no-cache';
			    config.headers['Pragma'] = 'no-cache';		
			}
			return config;
		},
		responseError: function(rejection) {
			if (rejection.status === 401) {
				delete $window.sessionStorage.token;
				isAuth = false;
			}
			return $q.reject(rejection);
		},
		setAuth: function(boolen) {
			isAuth = boolen;
		},
		getAuth: function() {
			return isAuth;
		}
	};
};