module.exports = function($scope, $http, $window, $location,$routeParams,authInterceptor) {

    var date = new Date();
    $scope.getMonth = function(month){    	
    	if (!month) {
    		month = date.getMonth()+1;
    	}
    	switch(month)
    	{
    		case 1: return {n: 1, name: 'Januar'};
    		case 2: return  {n: 2, name: 'Februar'};
    		case 3: return  {n: 3, name: 'März'};
    		case 4: return  {n: 4, name: 'April'};
    		case 5: return  {n: 5, name: 'Mai'};
    		case 6: return  {n: 6, name: 'Juni'};
    		case 7: return  {n: 7, name: 'Juli'};
    		case 8: return  {n: 8, name: 'August'};
    		case 9: return  {n: 9, name: 'September'};
    		case 10: return  {n: 10, name: 'Oktober'};
    		case 11: return  {n: 11, name: 'November'};
    		case 12: return  {n: 12, name: 'Dezember'};
    	}
    };
	$scope.go = function ( path ) {
	  $location.path( path );
	};
    $scope.getYear = function(){
        return date.getFullYear();
    }
};