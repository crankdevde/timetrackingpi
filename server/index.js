var express 		= require('express'),
	SerialPort		= require('serialport'),
	Datastore		= require('nedb'),
	request 		= require('request'),
	bodyParser		= require('body-parser'),
	cookieParser	= require('cookie-parser'),
	expressJwt		= require('express-jwt'),
	jwt 			= require('jsonwebtoken'),
	session 		= require('express-session'),
	server 			= express(),
	readerStatus	= {status:404,message:'not connected'},
	port 			= process.env.PORT || 8080,
	secret 			= '38alno08343jf34f@924�0934%39840$asdASD',
	sN 				= '',
	myPort			= {};


var allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers','Content-Type,Authorization','Cache-Control','If-Modified-Since','Pragma');
	next();
};
server.use(bodyParser.urlencoded({extended:true}))
	  .use(bodyParser.json())
	  .use(allowCrossDomain)
	  .use(session({
	  	secret: secret,
	  	apiKey: [],
	  	resave: false,
	  	saveUninitialized: true,
	  	cookie: {secure:true}
	  }))
	  .use(express.static('./dist/'))
	  .use('/api',expressJwt({secret: secret}))
	  .use(function(err,req,res,next){
	  	if (err.constructor.name === 'UnauthorizedError') {
	  		res.status(401).send('Unauthorized');
	  	}
	  })
	  .use('/',require('./routes/login').router)
	  .use('/',require('./routes/testdaten').router)
	  .use('/api',require('./routes/keys').router)
	  .use('/api',require('./routes/employee').router)
	  .use('/api',require('./routes/timeline').router)
	  .use('/api',require('./routes/rfid').router)
	  .use('/api',require('./routes/dyndns').router)
	  .use('/api/sn', function(req, res) {
		  res.json({sn:sN,status:readerStatus});
	  })
	  .all('/',function(req,res){
	  	res.sendFile('index.html', {root:'dist'});
	  })
	  .listen(8080);

function rfid(){
// list serial ports:
SerialPort.list(function (err, ports) {
  if ( err ) console.log(err);
  ports.forEach(function(port) {
  	if ( port.manufacturer == 'wch.cn' || port.manufacturer == '1a86') {
  		console.log(port);
	    myPort = new SerialPort(port.comName, {
		   baudRate: 9600
		});

	    myPort.on('open', showPortOpen);
		myPort.on('data', sendSerialData);
		myPort.on('close', showPortClose);
		myPort.on('error', showError);
	}

	function showPortOpen() {
	   console.log('port open. Data rate: ' + myPort.options.baudRate);
	   readerStatus = {status:200,message:'connected'};
	   loop();
	}
	 
	function sendSerialData(data) {

	   if( data.length == 7 ) {
			if ( sN === '' ){
				sNtmp = data.toString('hex',2,6);
				if ( sNtmp != '03030e03') {
					sN = sNtmp;
					myPort.write('\x02\x13\x15'); // sound
					var doc = { sn: sN, time: Date.now()};
					dbTime.insert(doc,function(err,newDoc){
						if( err ) console.log(err);
					});
				}
				clearReader();
			}
		}
	}
	 
	function showPortClose() {
	   console.log('port closed.');
	   myPort = null;
	   readerStatus = {status:404,message:'disconnected'};
	   reloadRfid();
	}
	 
	function showError(error) {
	   console.log('Serial port error: ' + error);
	   myPort = null;
	   readerStatus = {status:500,message:error};
	   reloadRfid();
	}

	function loop(){
		if ( myPort ) {
			setTimeout(function(){
			   if ( myPort !== null ) {
				    myPort.write('\x03\x02\x00\x05');
					myPort.write('\x02\x03\x05');
					loop();
				}
			},250);
		}
	}


	function clearReader(){
		setTimeout(function(){

	   		console.log(sN);
		   sN='';
	   },3000);
	}


  });
  if ( !myPort ) {
  	reloadRfid();
  }
});
}

function reloadRfid(){
	rfid();
}

function reloadDynDns(){
	request('https://carol.selfhost.de/update?username=USERNAME&password=PASSWORD', function (error, response, body) {
	    if (!error && response.statusCode == 200) {
	        console.log(body); // Print the google web page.
	     }
	});
	setTimeout(function(){
	   reloadDynDns();
   },43200000);
}


reloadDynDns();
rfid();

console.log('Magic happens on port ' + port);
