var express 		= require('express'),
	SerialPort		= require('serialport'),
	Datastore		= require('nedb'),
	request 		= require('request'),
	bodyParser		= require('body-parser'),
	cookieParser	= require('cookie-parser'),
	expressJwt		= require('express-jwt'),
	jwt 			= require('jsonwebtoken'),
	session 		= require('express-session'),
	router  		= express.Router();


	dbTime			= new Datastore({filename: 'time.db', autoload: true});

router 	.use(function(req,res,next){
			next();
		});

router 	.route('/timeline')
	 	.get(function(req, res){
	 		dbTime.find({}).sort({sn:1,time:1}).exec(function(err,docs) {
				if (err) console.log(err);
				res.json(docs);
			});
	 	});

router 	.route('/timeline/user')
	 	.get(function(req, res){
	 		dbTime.find({}).sort({sn:1,time:1}).exec(function(err,docs) {
				if (err) console.log(err);
				res.json(distinctField(docs,'sn'));
			});
	 	});

router 	.route('/timeline/:keyId')
	 	.get(function(req, res){
	 		dbTime.find({sn:req.params.keyId},function(err,docs) {
				if (err) console.log(err);
				res.json(docs);
			});
	 	});
router 	.route('/timeline/month/:keyId/:year/:month')
	 	.delete(function(req, res){
	 		var startDateString = req.params.year + '-' + req.params.month + '-01';
	 		var endDateString = req.params.year + '-' + req.params.month + '-' + daysInMonth(req.params.year,req.params.month);
	 		var startDate = new Date(startDateString);
	 		var endDate = new Date(endDateString);
	 		dbTime.remove({sn:req.params.keyId,time:{$gte:startDate.getTime(), $lte: endDate.getTime()}},{ multi: true },function(err,docs) {
				if (err) console.log(err);
				res.json(docs);
			});
	 	});
router 	.route('/timeline/days/:keyId/:year/:month')
	 	.get(function(req, res){
	 		var startDateString = req.params.year + '-' + req.params.month + '-01';
	 		var endDateString = req.params.year + '-' + req.params.month + '-' + daysInMonth(req.params.year,req.params.month);
	 		var startDate = new Date(startDateString);
	 		var endDate = new Date(endDateString);
	 		dbTime.find({sn:req.params.keyId,time:{$gte:startDate.getTime(), $lte: endDate.getTime()}}).sort({time:-1}).exec(function(err,docs) {
				if (err) console.log(err);
				res.json(docs);
			});
	 	});

module.exports.router = router;
function daysInMonth(iMonth, iYear)
{
    return 32 - new Date(iYear, iMonth-1, 32).getDate();
}
function distinctField(data,field){
	var distinctList={};
	for (var row in data) {
		distinctList[data[row][field]] = data[row][field];
	}
	return distinctList;
}