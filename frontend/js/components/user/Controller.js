module.exports = function($scope, $http, $window,$location,$routeParams,authInterceptor) {
	if (authInterceptor.getAuth()) {
		$location.path('/');
	}
	$scope.user = {username:'',password:''};
	$scope.result = {};
	$scope.sendLogin = function(){
		$http.post('/login',$scope.user)
		.then(function(result){
			console.log(result);
			if ( result.data === 'Wrong user or password' ) {
				$scope.result = {message:'login failed',status:401};
				delete $window.sessionStorage.token;
				authInterceptor.setAuth(false);
				$location.path('/');
			} else {
				$scope.result = result.data;
				$window.sessionStorage.token = result.data.token;
				authInterceptor.setAuth(true);
				$location.path('/');
			}
		});
	};
	$scope.logout = function() {
	    delete $window.sessionStorage.token;
	    authInterceptor.setAuth(false);
		$window.location.reload();
	};
	$scope.getAuth = function() {
		return authInterceptor.getAuth();
	};
	
	$scope.go = function ( path ) {
	  $location.path( path );
	};
};