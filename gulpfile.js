var gulp 		= require('gulp'),
	gutil 		= require('gulp-util'),
	server 		= require('gulp-express'),
	jshint 		= require('gulp-jshint'),
	validate 	= require('gulp-html-angular-validate'),
	templates  	= require('gulp-angular-templatecache'),
	uglify 		= require('gulp-uglify'),
	concat 		= require('gulp-concat'),
	minifyHTML 	= require('gulp-minify-html'),
	ngAnnotate 	= require('gulp-ng-annotate'),
	browserify 	= require('browserify'),
	del 		= require('del'),
	vinylPaths 	= require('vinyl-paths'),
	source 		= require('vinyl-source-stream'),
	cleanCSS 	= require('gulp-clean-css')
;

var paths = {
  web: 'frontend/',      // App root path
  server: 'server/',      // App root path
  dist: 'dist/', // Distribution path
  test: 'test/'     // Test path
};

gulp
	.task('dev',['jshint','html','css','browserify','templates','js'])
	.task('clean', function () {
	  return gulp
	  .src([paths.dist], {read: true})
	  .pipe(vinylPaths(del));
	})
	.task('jshint',function(){
		gulp.src(['./frontend/**/*.js','./frontend/**/.css','./server/**/.js'])
			.pipe(jshint())
			.pipe(jshint.reporter('default'));
	})
	.task('templates', function () {
	  // gulp.src([
	  //     './frontend/views/*.html',
	  //     '!./node_modules/**'
	  //   ])
	  //   .pipe(minifyHTML({
	  //     quotes: true
	  //   }))
	  //   .pipe(templates('templates.js'))
	  //   .pipe(gulp.dest(paths.dist+'js'));
	})
	.task('html',function(){
	  	 return gulp.src('./frontend/**/*.html')
    	  .pipe(minifyHTML({
		      quotes: true
		    }))
    	  .pipe(gulp.dest(paths.dist));
	})
	.task('css',function(){
		return gulp.src('frontend/css/*.css')
	    .pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
	    .pipe(gulp.dest('dist/css'));
	})
	.task('browserify', ['jshint'], function () {
	  return browserify(paths.web + '/js/app.js', {debug: true})
	  .bundle()
	  .pipe(source('app.js'))
	  .pipe(gulp.dest(paths.dist+'js/'));
	})
	.task('js',function(){
		gulp.src(['server/**/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(gulp.dest('dist'));
	})
	.task('server',['dev'],function(){
		server.run(['dist/index.js']);
	})
	.task('serverstop',function(){
		server.stop();
	})
	.task('build',function(){

	})
	.task('watch',['dev'], function(){
		gulp.watch(['./frontend/**/*.js'],['dev']);
		gulp.watch(['./server/**/*.js'],['dev']);
		gulp.watch(['./frontend/css/**/*.css'],['dev']);
		gulp.watch(['./frontend/**/*.html'],['dev']);
		gulp.watch(['./server/**/*.js'], [server.notify]);
	})
	.task('default',['clean','dev','server','watch'])
;
