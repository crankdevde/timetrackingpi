module.exports = function($scope, $http, $window,$location,$routeParams) {
	$scope.paramKeyId = $routeParams.keyId;
	$scope.paramYear = $routeParams.year;
	$scope.paramMonth = $routeParams.month;
	$scope.deDays = {'Mon':'Mo','Tue':'Di','Wed':'Mi','Thu':'Do','Fri':'Fr','Sat':'Sa','Sun':'So'};
	$scope.getEmployeeTimeLine = function() {
		$http.get("/api/timeline").then(function (response) {
	        $scope.listAll = getTimeLineMonth(response.data);
		});
	};
	$scope.getEmployeeTimeLine();
	$scope.getEmployeeMonth = function(keyID) {
		$http.get("/api/timeline/month/"+keyID).then(function (response) {
	        $scope.listMonth = getTimeLineMonth(response.data);
		});
	};
	$scope.getEmployeeDaysInMonth = function(keyID,month,year) {
		console.log(keyID,month,year);
		console.log('getEmployeeDaysInMonth called');
		$http.get("/api/timeline/days/"+keyID+'/'+year+'/'+month).then(function (response) {
	        $scope.listDays = getTimeLineDays(response.data);
	        console.log($scope.listDays);
		});
	};
	if ($scope.paramKeyId && $scope.paramMonth && $scope.paramYear) {
		$scope.getEmployeeDaysInMonth($scope.paramKeyId,$scope.paramMonth,$scope.paramYear);
	}
	$scope.go = function ( path ) {
	  $location.path( path );
	};
	$scope.deleteMonth = function(paramKeyId,paramMonth,paramYear) {
		$http.delete("/api/timeline/month/"+paramKeyId +"/" +paramYear+"/" +paramMonth).then(function (response) {
			$location.path('/timeline');
		});
	};
};


function getTimeLineDays(jsonData)
{
	var resultSet = {data:{},totalMS:0,lastLog:0,timediff:{day:0,hour:0,minute:0, second: 0, miliseconds:0}};
	var returnSet = {};
	var rowCounter=0
	var oldRow= {time:0,timediff:{day:0,hour:0,minute:0, second: 0, miliseconds:0}};
	for(var row in jsonData ) {
		
		jsonData[row].display = true;
		if ( ! resultSet.data[dateFormat(jsonData[row].time, "isoDate")]) {
			rowCounter = 0;
			resultSet.data[dateFormat(jsonData[row].time, "isoDate")]={rows:[],ms:0,timediff:{day:0,hour:0,minute:0, second: 0, miliseconds:0}};
		}	

		jsonData[row].startDate = jsonData[row].time;
		if(rowCounter%2)  {
			if ( resultSet.lastLog === 0 ) resultSet.lastLog = jsonData[row].time;
			oldRow.display = false;
			jsonData[row].timediff = timeDifference(jsonData[row].time,oldRow.time);
			jsonData[row].display = true;
			jsonData[row].endDate = oldRow.time;
			jsonData[row].startDate = jsonData[row].time;
			resultSet.data[dateFormat(jsonData[row].time, "isoDate")].ms += (oldRow.time-jsonData[row].time);
			resultSet.data[dateFormat(jsonData[row].time, "isoDate")].timediff = timeDifference(new Date(),new Date()-resultSet.data[dateFormat(jsonData[row].time, "isoDate")].ms);
			resultSet.totalMS += (oldRow.time-jsonData[row].time);
		}
		resultSet.data[dateFormat(jsonData[row].time, "isoDate")].rows.push(jsonData[row]);
		oldRow = jsonData[row];

		rowCounter++;
	}
	resultSet.timediff = timeDifference(new Date(),new Date()-resultSet.totalMS)
	return resultSet;
}
function getTimeLineMonth(jsonData)
{
	var resultSet = {};
	for(var row in jsonData ) {
		if ( ! resultSet[jsonData[row].sn])	resultSet[jsonData[row].sn] ={};
		if ( ! resultSet[jsonData[row].sn][dateFormat(row.time, "yyyy")])	resultSet[jsonData[row].sn][dateFormat(row.time, "yyyy")] ={};
		if ( ! resultSet[jsonData[row].sn][dateFormat(row.time, "yyyy")][dateFormat(row.time, "mm")])	resultSet[jsonData[row].sn][dateFormat(row.time, "yyyy")][dateFormat(row.time, "mm")] =[];
		
		resultSet[jsonData[row].sn][dateFormat(row.time, "yyyy")][dateFormat(row.time, "mm")].push(jsonData[row]);
	}
	return resultSet;
}

var delta =0;
function timeDifference(d1,d2) {
	date1 = new Date(d1);
	date2 = new Date(d2);

    // get total seconds between the times
	delta = Math.abs(date1 - date2) / 1000;

	// calculate (and subtract) whole days
	var days = Math.floor(delta / 86400);
	delta -= days * 86400;

	// calculate (and subtract) whole hours
	var hours = Math.floor(delta / 3600) % 24;
	delta -= hours * 3600;

	// calculate (and subtract) whole minutes
	var minutes = Math.floor(delta / 60) % 60;
	delta -= minutes * 60;

	// what's left is seconds
	var seconds = Math.round(delta % 60);

    return {day:days,hour:hours,minute:minutes, second: seconds, miliseconds:0};
}
// dateFormat(row.time, "yyyy-mm")

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};
