var express 		= require('express'),
	SerialPort		= require('serialport'),
	Datastore		= require('nedb'),
	request 		= require('request'),
	bodyParser		= require('body-parser'),
	cookieParser	= require('cookie-parser'),
	expressJwt		= require('express-jwt'),
	jwt 			= require('jsonwebtoken'),
	session 		= require('express-session'),
	router  		= express.Router(),
	secret 			= '38alno08343jf34f@924€0934%39840$asdASD';

router 	.use(function(req,res,next){
			next();
		})

	 	.route('/login')
	 	.get(function(req,res){
	 		res.json({status:500,message:'only post'});
	 	})
	 	.post(function(req, res){
	 		if (!(req.body.username === 'pi' && req.body.password === 'timetracking')) {
		      res.status(401).send('Wrong user or password');
		      return;
		    }
		    req.session.user = req.body.username;
		    // We are sending the doc inside the token
		    var token = jwt.sign(req.body, secret, { expiresIn : 60*60*24 });
		    res.json({ token: token });

	 	});
module.exports.router = router;